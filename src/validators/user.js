'use strict';
const Joi = require('joi')

const createUserValidator = Joi.object({
  name: Joi.string().min(5).required(),
  account: Joi.string().required(),
  cpf: Joi.string().length(14).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required(),
  phones: Joi.array().items(Joi.object({
    number: Joi.string().min(10).max(11).required(),
    type: Joi.valid('cellphone', 'telephone').required()
  })).min(1).required()
})

const editUserValidator = Joi.object({
  name: Joi.string().min(5),
  cpf: Joi.string().length(14),
  email: Joi.string().email(),
  password: Joi.string().min(8),
  phones: Joi.array().items(Joi.object({
    number: Joi.string().min(10).max(11).required(),
    type: Joi.valid('cellphone', 'telephone').required()
  })).min(1)
})

const deleteUserValidator = Joi.object({
  active: Joi.boolean().required()
})

function createUser (data) {
  return createUserValidator.validateAsync(data, { abortEarly: false })
}

function editUser (data) {
  return editUserValidator.validateAsync(data, { abortEarly: false })
}

function deleteUser (data) {
  return deleteUserValidator.validateAsync(data, { abortEarly: false })
}

module.exports = {
  createUser,
  editUser,
  deleteUser
}
