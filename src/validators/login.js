'use strict';
const Joi = require('joi')

const loginValidator = Joi.object({
  account: Joi.string().length(13).required(),
  password: Joi.string().min(8).required()
})

function login (data) {
  return loginValidator.validateAsync(data, { abortEarly: false })
}

module.exports = {
  login
}
